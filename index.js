let http = require("http");

let port = 4000;



http.createServer((request, response) =>{

	if (request.url == "/" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}else if (request.url == "/profile" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile');
	}else if (request.url == "/courses" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Here\'s our courses available');
	}else if (request.url == "/addCourses" &&  request.method =="POST") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Added courses to our resources');
	}else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Page not available!');
	}

}).listen(port);

console.log(`Server is running at localhost: ${port}`);




